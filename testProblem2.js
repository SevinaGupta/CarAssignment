//import {problem} from './problem2.mjs'

const inventory = require('./car_data');
const problem = require('./problem2.js');

let value = problem(inventory);  // return last value
problem({});                     // return undefined


console.log(`last car is ${value['car_year']} ${value['car_make']} ${value['car_model']}`);